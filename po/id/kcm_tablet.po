# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Wantoyèk <wantoyek@gmail.com>, 2022.
# Linerly <linerly@protonmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-19 00:48+0000\n"
"PO-Revision-Date: 2023-01-13 08:52+0700\n"
"Last-Translator: Linerly <linerly@protonmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Utama (default)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Potret"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Lanskap"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Potret Terbalik"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Lanskap Terbalik"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Ikuti layar yang aktif"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "Pas ke Keluaran"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Pas Keluaran dalam tablet"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Ukuran kustom"

#: package/contents/ui/main.qml:31
#, kde-format
msgid "No drawing tablets found."
msgstr "Tidak ada tablet gambar yang ditemukan."

#: package/contents/ui/main.qml:39
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Peranti:"

#: package/contents/ui/main.qml:77
#, kde-format
msgid "Target display:"
msgstr "Target layar:"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Orientation:"
msgstr "Orientasi:"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Left-handed mode:"
msgstr "Mode tangan kiri:"

#: package/contents/ui/main.qml:118
#, kde-format
msgid "Area:"
msgstr "Area:"

#: package/contents/ui/main.qml:206
#, kde-format
msgid "Resize the tablet area"
msgstr "Ubah ukuran area tablet"

#: package/contents/ui/main.qml:230
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Kuncu rasio aspek"

#: package/contents/ui/main.qml:238
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Tool Button 1"
msgstr "Tombol Alat 1"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Tool Button 2"
msgstr "Tombol Alat 2"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Tool Button 3"
msgstr "Tombol Alat 3"

#: package/contents/ui/main.qml:291
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Pad:"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "None"
msgstr "Nihil"

#: package/contents/ui/main.qml:324
#, kde-format
msgid "Button %1:"
msgstr "Tombol %1:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Wantoyèk"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wantoyek@gmail.com"

#~ msgid "Tablet"
#~ msgstr "Tablet"

#~ msgid "Configure drawing tablets"
#~ msgstr "Konfugurasi tablet gambar"
