# Translation of kcmkclock into Japanese.
# This file is distributed under the same license as the kdebase package.
# Taiki Komoda <kom@kde.gr.jp>, 2004, 2010.
# Shinichi Tsunoda <tsuno@ngy.1st.ne.jp>, 2005.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007, 2008, 2009.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2005, 2010, 2015, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-27 00:49+0000\n"
"PO-Revision-Date: 2022-08-20 23:06-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Noboru Sinohara, Fumiaki Okushi,Shinichi Tsunoda,Ryuichi Yamada"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"shinobo@leo.bekkoame.ne.jp, fumiaki@okushi.com,tsuno@ngy.1st.ne.jp,"
"ryuichi_ya220@outlook.jp"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: dateandtime.ui:22
#, kde-format
msgid "Date and Time"
msgstr "日付と時刻"

#. i18n: ectx: property (text), widget (QCheckBox, setDateTimeAuto)
#: dateandtime.ui:30
#, kde-format
msgid "Set date and time &automatically"
msgstr "日付と時刻を自動的に設定する(&A)"

#. i18n: ectx: property (text), widget (QLabel, timeServerLabel)
#: dateandtime.ui:53
#, kde-format
msgid "&Time server:"
msgstr "タイムサーバ(&T):"

#. i18n: ectx: property (whatsThis), widget (KDatePicker, cal)
#: dateandtime.ui:86
#, kde-format
msgid "Here you can change the system date's day of the month, month and year."
msgstr "ここでシステムの日付を変更します。"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: dateandtime.ui:122
#, kde-format
msgid "Time Zone"
msgstr "タイムゾーン"

#. i18n: ectx: property (text), widget (QLabel, label)
#: dateandtime.ui:128
#, kde-format
msgid "To change the local time zone, select your area from the list below."
msgstr ""
"ローカルタイムゾーンを変更するには、下のリストからエリアを選択してください。"

#. i18n: ectx: property (text), widget (QLabel, m_local)
#: dateandtime.ui:151
#, kde-format
msgid "Current local time zone:"
msgstr "現在のローカルタイムゾーン:"

#. i18n: ectx: property (placeholderText), widget (KTreeWidgetSearchLine, tzonesearch)
#: dateandtime.ui:161
#, kde-format
msgid "Search…"
msgstr "検索..."

#: dtime.cpp:61
#, kde-format
msgid ""
"No NTP utility has been found. Install 'ntpdate' or 'rdate' command to "
"enable automatic updating of date and time."
msgstr ""
"NTP ユーティリティが見つかりませんでした。日付と時刻の自動更新を有効にするに"
"は、'ntpdate' か 'rdate' をインストールしてください。"

#: dtime.cpp:91
#, kde-format
msgid ""
"Here you can change the system time. Click into the hours, minutes or "
"seconds field to change the relevant value, either using the up and down "
"buttons to the right or by entering a new value."
msgstr ""
"ここでシステムの時刻を変更します。時、分、秒フィールドをクリックし、各フィー"
"ルドの右端の上下のボタンを使うか、直接入力して値を変更してください。"

#: dtime.cpp:113
#, kde-format
msgctxt "%1 is name of time zone"
msgid "Current local time zone: %1"
msgstr "現在のローカルタイムゾーン: %1"

#: dtime.cpp:116
#, kde-format
msgctxt "%1 is name of time zone, %2 is its abbreviation"
msgid "Current local time zone: %1 (%2)"
msgstr "現在のローカルタイムゾーン: %1 (%2)"

#: dtime.cpp:203
#, kde-format
msgid ""
"Public Time Server (pool.ntp.org),        asia.pool.ntp.org,        europe."
"pool.ntp.org,        north-america.pool.ntp.org,        oceania.pool.ntp.org"
msgstr ""
"公共のタイムサーバ (pool.ntp.org), asia.pool.ntp.org, europe.pool.ntp.org, "
"north-america.pool.ntp.org, oceania.pool.ntp.org"

#: dtime.cpp:274
#, kde-format
msgid "Unable to contact time server: %1."
msgstr "タイムサーバに接続できません: %1"

#: dtime.cpp:278
#, kde-format
msgid "Can not set date."
msgstr "日付を設定できません。"

#: dtime.cpp:281
#, kde-format
msgid "Error setting new time zone."
msgstr "新しいタイムゾーンへの切り替え時にエラーが発生しました。"

#: dtime.cpp:281
#, kde-format
msgid "Time zone Error"
msgstr "タイムゾーンのエラー"

#: dtime.cpp:299
#, kde-format
msgid ""
"<h1>Date & Time</h1> This system settings module can be used to set the "
"system date and time. As these settings do not only affect you as a user, "
"but rather the whole system, you can only change these settings when you "
"start the System Settings as root. If you do not have the root password, but "
"feel the system time should be corrected, please contact your system "
"administrator."
msgstr ""
"<h1>日付と時間</h1>このシステム設定モジュールでシステムの日付と時間を調整しま"
"す。これはユーザであるあなただけでなく、システム全体に影響を及ぼすので、シス"
"テム設定を root として起動したときのみ調整できます。あなたに調整する権限がな"
"い場合は、システム管理者に連絡してください。"

#: main.cpp:49
#, kde-format
msgid "KDE Clock Control Module"
msgstr "KDE 日付と時刻の設定モジュール"

#: main.cpp:53
#, kde-format
msgid "(c) 1996 - 2001 Luca Montecchiani"
msgstr "(c) 1996 - 2001 Luca Montecchiani"

#: main.cpp:55
#, kde-format
msgid "Luca Montecchiani"
msgstr "Luca Montecchiani"

#: main.cpp:55
#, kde-format
msgid "Original author"
msgstr "オリジナルの作者"

#: main.cpp:56
#, kde-format
msgid "Paul Campbell"
msgstr "Paul Campbell"

#: main.cpp:56
#, kde-format
msgid "Current Maintainer"
msgstr "現在のメンテナ"

#: main.cpp:57
#, kde-format
msgid "Benjamin Meyer"
msgstr "Benjamin Meyer"

#: main.cpp:57
#, kde-format
msgid "Added NTP support"
msgstr "NTP サポートの追加"

#: main.cpp:60
#, kde-format
msgid ""
"<h1>Date & Time</h1> This control module can be used to set the system date "
"and time. As these settings do not only affect you as a user, but rather the "
"whole system, you can only change these settings when you start the System "
"Settings as root. If you do not have the root password, but feel the system "
"time should be corrected, please contact your system administrator."
msgstr ""
"<h1>日付と時間</h1>この設定モジュールでシステムの日付と時間を調整します。これ"
"はユーザであるあなただけでなく、システム全体に影響を及ぼすので、システム設定"
"を root として起動したときのみ調整できます。もし root のパスワードを知らな"
"く、時間を調整したい場合は、システム管理者に連絡してください。"

#: main.cpp:113
#, kde-format
msgid "Unable to authenticate/execute the action: %1, %2"
msgstr "認証できないかアクションを実行できません: %1, %2"

#: main.cpp:133
#, kde-format
msgid "Unable to change NTP settings"
msgstr "NTP の設定を変更できませんでした"

#: main.cpp:144
#, kde-format
msgid "Unable to set current time"
msgstr "現在時間を設定できません"

#: main.cpp:154
#, kde-format
msgid "Unable to set timezone"
msgstr "タイムゾーンを設定できません"
