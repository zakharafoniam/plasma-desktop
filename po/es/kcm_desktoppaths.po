# Translation of kcmkonq to Spanish
# translation of kcmkonq.po to Spanish
# traducción de kcmkonq.po a Español
# Translation to spanish.
# Copyright (C) 2000-2002.
#
# Pablo de Vicente <vicente@oan.es>,2000-2002.
# Jaime Robles <jaime@kde.org>, 2003, 2007, 2008.
# Pablo de Vicente <vicnte@oan.es>, 2003.
# Miguel Revilla Rodríguez <yo@miguelrevilla.com>, 2004.
# Pablo de Vicente <p.devicente@wanadoo.es>, 2004, 2005.
# Juan Manuel Garcia Molina <juanma@superiodico.net>, 2005.
# Pablo de Vicente <pablo.devicente@gmail.com>, 2005, 2006.
# Enrique Matias Sanchez (aka Quique) <cronopios@gmail.com>, 2007.
# Eloy Cuadra <ecuadra@eloihr.net>, 2008, 2009, 2014, 2017, 2020, 2021, 2022.
# Javier Vinal <fjvinal@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: kcmkonq\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-06 01:02+0000\n"
"PO-Revision-Date: 2022-12-01 16:38+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"com>\n"
"First-Translator: Boris Wesslowski <Boris@Wesslowski.com>\n"
"X-Generator: Lokalize 22.11.90\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: desktoppathssettings.cpp:211
#, kde-format
msgid "Desktop"
msgstr "Escritorio"

#: desktoppathssettings.cpp:226
#, kde-format
msgid "Documents"
msgstr "Documentos"

#: desktoppathssettings.cpp:241
#, kde-format
msgid "Downloads"
msgstr "Descargas"

#: desktoppathssettings.cpp:256
#, kde-format
msgid "Music"
msgstr "Música"

#: desktoppathssettings.cpp:271
#, kde-format
msgid "Pictures"
msgstr "Imágenes"

#: desktoppathssettings.cpp:286
#, kde-format
msgid "Videos"
msgstr "Vídeos"

#: desktoppathssettings.cpp:301
#, kde-format
msgid "Public"
msgstr "Público"

#: desktoppathssettings.cpp:316
#, kde-format
msgid "Templates"
msgstr "Plantillas"

#: globalpaths.cpp:27
#, kde-format
msgid ""
"<h1>Paths</h1>\n"
"This module allows you to choose where in the filesystem the files on your "
"desktop should be stored.\n"
"Use the \"Whats This?\" (Shift+F1) to get help on specific options."
msgstr ""
"<h1>Rutas</h1>\n"
"Este módulo le permite elegir en qué parte del sistema de archivos se "
"almacenan los archivos del escritorio.\n"
"Use «¿Qué es esto?» (Mayúsculas+F1) para obtener ayuda sobre las opciones "
"específicas."

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Desktop path:"
msgstr "Ruta del escritorio:"

#: package/contents/ui/main.qml:25
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"Esta carpeta contiene todos los archivos que ve en su escritorio. Se puede "
"cambiar la ubicación de esta carpeta si así lo desea, y los contenidos "
"también serán transferidos automáticamente a la nueva dirección."

#: package/contents/ui/main.qml:31
#, kde-format
msgid "Documents path:"
msgstr "Ruta para documentos:"

#: package/contents/ui/main.qml:34
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr "Esta carpeta será la predeterminada para cargar o guardar documentos."

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Downloads path:"
msgstr "Ruta para descargas:"

#: package/contents/ui/main.qml:43
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr ""
"Esta carpeta será la predeterminada para guardar los elementos que descargue."

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Videos path:"
msgstr "Ruta para los vídeos:"

#: package/contents/ui/main.qml:52 package/contents/ui/main.qml:79
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr "Esta carpeta será la predeterminada para cargar o guardar películas."

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Pictures path:"
msgstr "Ruta para imágenes:"

#: package/contents/ui/main.qml:61
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr "Esta carpeta será la predeterminada para cargar o guardar imágenes."

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Music path:"
msgstr "Ruta para música:"

#: package/contents/ui/main.qml:70
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr "Esta carpeta será la predeterminada para cargar o guardar música."

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Public path:"
msgstr "Ruta de uso público:"

#: package/contents/ui/main.qml:85
#, kde-format
msgid "Templates path:"
msgstr "Ruta para plantillas:"

#: package/contents/ui/main.qml:88
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr "Esta carpeta será la predeterminada para cargar o guardar plantillas."

#: package/contents/ui/UrlRequester.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr ""

#~ msgid ""
#~ "This folder will be used by default to load or save public shares from or "
#~ "to."
#~ msgstr ""
#~ "Esta carpeta será la predeterminada para cargar o guardar elementos "
#~ "compartidos públicamente."

#~ msgid "Autostart path:"
#~ msgstr "Ruta de inicio automático:"

#~ msgid ""
#~ "This folder contains applications or links to applications (shortcuts) "
#~ "that you want to have started automatically whenever the session starts. "
#~ "You can change the location of this folder if you want to, and the "
#~ "contents will move automatically to the new location as well."
#~ msgstr ""
#~ "Esta carpeta contiene aplicaciones o enlaces a aplicaciones (accesos "
#~ "rápidos) que se iniciarán automáticamente cuando arranque la sesión. Se "
#~ "puede cambiar la ubicación de esta carpeta si así lo desea, y los "
#~ "contenidos también serán transferidos automáticamente a la nueva "
#~ "dirección."

#~ msgid "Autostart"
#~ msgstr "Inicio automático"

#~ msgid "Movies"
#~ msgstr "Películas"

#~ msgid ""
#~ "The path for '%1' has been changed.\n"
#~ "Do you want the files to be moved from '%2' to '%3'?"
#~ msgstr ""
#~ "La ruta de «%1» ha cambiado.\n"
#~ "¿Desea mover los archivos de «%2» a «%3»?"

#~ msgctxt "Move files from old to new place"
#~ msgid "Move"
#~ msgstr "Mover"

#~ msgctxt "Use the new directory but do not move files"
#~ msgid "Do not Move"
#~ msgstr "No mover"

#~ msgid ""
#~ "The path for '%1' has been changed.\n"
#~ "Do you want to move the directory '%2' to '%3'?"
#~ msgstr ""
#~ "La ruta de «%1» ha cambiado.\n"
#~ "¿Desea mover la carpeta «%2» a «%3»?"

#~ msgctxt "Move the directory"
#~ msgid "Move"
#~ msgstr "Mover"

#~ msgctxt "Use the new directory but do not move anything"
#~ msgid "Do not Move"
#~ msgstr "No mover"

#~ msgid "Confirmation Required"
#~ msgstr "Se necesita confirmación"
