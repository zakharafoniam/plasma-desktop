# Translation of kaccess to Northern Sami
#
# Børre Gaup <boerre@skolelinux.no>, 2002, 2003, 2004, 2005, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-28 00:51+0000\n"
"PO-Revision-Date: 2007-12-22 19:04+0100\n"
"Last-Translator: Børre Gaup <boerre@skolelinux.no>\n"
"Language-Team: Northern Sami <l10n-no@lister.huftis.org>\n"
"Language: se\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Børre Gaup"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "boerre@skolelinux.no"

#: kaccess.cpp:69
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Shift-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:70
msgid "The Shift key is now active."
msgstr "Shift-boallu lea aktiivalaš"

#: kaccess.cpp:71
msgid "The Shift key is now inactive."
msgstr "Shift-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:75
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Ctrl-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:76
msgid "The Control key is now active."
msgstr "Ctrl-boallu lea aktiivalaš."

#: kaccess.cpp:77
msgid "The Control key is now inactive."
msgstr "Ctrl-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:81
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Alt-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:82
msgid "The Alt key is now active."
msgstr "Alt-boallu lea aktiivalaš."

#: kaccess.cpp:83
msgid "The Alt key is now inactive."
msgstr "Alt-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:87
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Win-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:88
msgid "The Win key is now active."
msgstr "Win-boallu lea aktiivalaš."

#: kaccess.cpp:89
msgid "The Win key is now inactive."
msgstr "Win-boallu ii lea šat aktiivalaš."

#: kaccess.cpp:93
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Meta-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:94
msgid "The Meta key is now active."
msgstr "Meta-boallu lea aktiivalaš."

#: kaccess.cpp:95
msgid "The Meta key is now inactive."
msgstr "Meta-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:99
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Super-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:100
msgid "The Super key is now active."
msgstr "Super-boallu lea aktiivalaš."

#: kaccess.cpp:101
msgid "The Super key is now inactive."
msgstr "Super-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:105
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Hyper-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:106
msgid "The Hyper key is now active."
msgstr "Hyper-boallu lea aktiivalaš."

#: kaccess.cpp:107
msgid "The Hyper key is now inactive."
msgstr "Hyper-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:111
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Alt Gr-boallu lea lohkkaduvvon ja lea dál aktiivalaš čuovvuvaš "
"boallodeaddilemiide."

#: kaccess.cpp:112
msgid "The Alt Graph key is now active."
msgstr "Alt Gr-boallu lea aktiivalaš."

#: kaccess.cpp:113
msgid "The Alt Graph key is now inactive."
msgstr "Alt Gr-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:114
msgid "The Num Lock key has been activated."
msgstr "Num Lock-boallu lea dál aktiverejuvvon."

#: kaccess.cpp:114
msgid "The Num Lock key is now inactive."
msgstr "Num Lock-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:115
msgid "The Caps Lock key has been activated."
msgstr "Caps Lock-boallu lea aktiverejuvvon."

#: kaccess.cpp:115
msgid "The Caps Lock key is now inactive."
msgstr "Caps Lock-boallu ii leat šat aktiivalaš."

#: kaccess.cpp:116
msgid "The Scroll Lock key has been activated."
msgstr "Scroll Lock-boallu lea aktiverejuvvon."

#: kaccess.cpp:116
msgid "The Scroll Lock key is now inactive."
msgstr "Scroll Lock ii leat šat aktiivalaš."

#: kaccess.cpp:327
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr ""

#: kaccess.cpp:329
#, kde-format
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr ""

#: kaccess.cpp:615
#, kde-format
msgid "AltGraph"
msgstr "Alt Graph"

#: kaccess.cpp:617
#, kde-format
msgid "Hyper"
msgstr "Hyper"

#: kaccess.cpp:619
#, kde-format
msgid "Super"
msgstr "Super"

#: kaccess.cpp:621
#, kde-format
msgid "Meta"
msgstr "Meta"

#: kaccess.cpp:638
#, kde-format
msgid "Warning"
msgstr "Váruhus"

#: kaccess.cpp:666
#, kde-format
msgid "&When a gesture was used:"
msgstr ""

#: kaccess.cpp:672
#, kde-format
msgid "Change Settings Without Asking"
msgstr "Rievdat heivehusaid jearakeahttá"

#: kaccess.cpp:673
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr "Čájet dán nannenláseža"

#: kaccess.cpp:674
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr ""

#: kaccess.cpp:717 kaccess.cpp:719
#, kde-format
msgid "Slow keys"
msgstr "Njoahces boalut"

#: kaccess.cpp:722 kaccess.cpp:724
#, kde-format
msgid "Bounce keys"
msgstr ""

#: kaccess.cpp:727 kaccess.cpp:729
#, kde-format
msgid "Sticky keys"
msgstr "Giddes modifikáhtorboaluid"

#: kaccess.cpp:732 kaccess.cpp:734
#, kde-format
msgid "Mouse keys"
msgstr "Sáhpánboalut"

#: kaccess.cpp:741
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr ""

#: kaccess.cpp:744
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr ""

#: kaccess.cpp:748
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr ""

#: kaccess.cpp:751
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr ""

#: kaccess.cpp:762
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr "Háliidatgo duođas aktiveret «%1»?"

#: kaccess.cpp:765
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr ""

#: kaccess.cpp:768
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr ""

#: kaccess.cpp:774
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr ""

#: kaccess.cpp:785
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr "Háliidatgo duođas aktiveret «%1» ja «%2»?"

#: kaccess.cpp:788
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr ""

#: kaccess.cpp:794
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr ""

#: kaccess.cpp:805
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr "Háliidatgo duođas aktiveret «%1», «%2» ja «%3»?"

#: kaccess.cpp:808
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr ""

#: kaccess.cpp:817
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "Háliidatgo duođas aktiveret «%1», «%2», «%3» ja «%4»?"

#: kaccess.cpp:826
#, kde-format
msgid "An application has requested to change this setting."
msgstr "Prográmma lea jearran jus oažžu rievdadit dán heivehusa."

#: kaccess.cpp:830
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""
"Don leat doallan Shift-boalu eanetgo 8 sekundda, dahje prográmma lea jearran "
"jus oažžu rievdadit dán heivehusa."

#: kaccess.cpp:832
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""

#: kaccess.cpp:836
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr ""

#: kaccess.cpp:841
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""

#: kaccess.cpp:843
#, kde-format
msgid "An application has requested to change these settings."
msgstr ""

#: kaccess.cpp:848
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""

#: kaccess.cpp:869
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""

#: kaccess.cpp:871
#, kde-format
msgid "Slow keys has been disabled."
msgstr ""

#: kaccess.cpp:875
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""

#: kaccess.cpp:877
#, kde-format
msgid "Bounce keys has been disabled."
msgstr ""

#: kaccess.cpp:881
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""

#: kaccess.cpp:883
#, kde-format
msgid "Sticky keys has been disabled."
msgstr ""

#: kaccess.cpp:887
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""

#: kaccess.cpp:889
#, kde-format
msgid "Mouse keys has been disabled."
msgstr ""

#: main.cpp:49
#, kde-format
msgid "Accessibility"
msgstr ""

#: main.cpp:49
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "© 2000 Matthias Hölzer-Klüpfel"

#: main.cpp:51
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hölzer-Klüpfel"

#: main.cpp:51
#, kde-format
msgid "Author"
msgstr "Čálli"
