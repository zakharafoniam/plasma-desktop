# translation of org.kde.plasma.emojier.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2019.
# Matej Mrenica <matejm98mthw@gmail.com>, 2020, 2021, 2022, 2023.
# Dušan Kazik <prescott66@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: org.kde.plasma.emojier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-31 01:00+0000\n"
"PO-Revision-Date: 2023-01-11 11:56+0100\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "Výber emoji"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík,Dušan Kazik"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com,prescott66@gmail.com"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "Nahradiť existujúcu inštanciu"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:52
#, kde-format
msgid "Search"
msgstr "Hľadať"

#: app/ui/CategoryPage.qml:80
#, kde-format
msgid "Clear History"
msgstr "Vyčistiť históriu"

#: app/ui/CategoryPage.qml:104
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Character"
msgstr ""

#: app/ui/CategoryPage.qml:107 app/ui/CategoryPage.qml:115
#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 skopírované do schránky"

#: app/ui/CategoryPage.qml:112
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Description"
msgstr ""

#: app/ui/CategoryPage.qml:205
#, kde-format
msgid "No recent Emojis"
msgstr "Žiadne nedávne emotikony"

#: app/ui/emojier.qml:42
#, kde-format
msgid "Recent"
msgstr "Nedávne"

#: app/ui/emojier.qml:63
#, kde-format
msgid "All"
msgstr "Všetko"

#: app/ui/emojier.qml:71
#, kde-format
msgid "Categories"
msgstr "Kategórie"

#: emojicategory.cpp:14
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr ""

#: emojicategory.cpp:15
msgctxt "Emoji Category"
msgid "People and Body"
msgstr "Ľudia a telo"

#: emojicategory.cpp:16
msgctxt "Emoji Category"
msgid "Component"
msgstr "Komponent"

#: emojicategory.cpp:17
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr "Zvieratá a príroda"

#: emojicategory.cpp:18
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr "Jedlo a pitie"

#: emojicategory.cpp:19
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr "Cestovanie a miesta"

#: emojicategory.cpp:20
msgctxt "Emoji Category"
msgid "Activities"
msgstr "Aktivity"

#: emojicategory.cpp:21
msgctxt "Emoji Category"
msgid "Objects"
msgstr "Objekty"

#: emojicategory.cpp:22
msgctxt "Emoji Category"
msgid "Symbols"
msgstr "Symboly"

#: emojicategory.cpp:23
msgctxt "Emoji Category"
msgid "Flags"
msgstr "Vlajky"

#~ msgid "Search…"
#~ msgstr "Hľadať..."

#~ msgid "Search..."
#~ msgstr "Hľadať..."
